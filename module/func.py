from django.conf import settings
from linebot import LineBotApi
from linebot.models import TextSendMessage, ImageSendMessage
import requests
import pandas as pd
import os,io
import datetime
import time
import sqlite3
import glob
import matplotlib.pyplot as plt
import numpy as np
#抓股價的套件
import yfinance as yf

line_bot_api = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)

def sendUse(event):
    try:
        text1='''!這是提供台股與美股數據的機器人!'''

        message=TextSendMessage(
            text=text1
        )
        line_bot_api.reply_message(event.reply_token, message)
    except:
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='發生錯誤！'))


def sendScrachStock(event,mtext):
    try:
        
        #新版
        #股票代號
        stock_no= mtext.upper()
        #起始日期
        start_date='2021-01-01'
        #下載資料
        df=yf.download(stock_no,start=start_date)
        #最新5筆資料
        result=df.tail()
     
        text1='查詢 {0} 股票資料 \n {1}'.format(mtext.upper(),result)
        message = [
            TextSendMessage(  
                text = text1
            )
        ]
        line_bot_api.reply_message(event.reply_token, message)
    except Exception as e:
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='發生錯誤！ \n {0}'.format(e)))